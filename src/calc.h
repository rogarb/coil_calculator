/* calc.h - defines the functions necessary for the parameter calculation */
#ifndef CALC_H
#define CALC_H

#include "coil.h"

enum FORMULA { BROOKS = 1,	/* brooks formula for square section */
	       WHEELER_S,	/* wheeler for single layer solenoid */
	       WHEELER_M,	/* wheeler for multiple layer solenoid */
	       NUM_FORMULA };

/* returns the raw value of the modifier */
double modifier_value(const int modifier);

/* returns the value stored in data transformed with the right modifier */
double calculate(const struct data_t data, const int to_modifier);

/* returns 0 on success, < 0 on failure */
int calculate_coil(struct coil_t *coil, enum FORMULA);

/* returns the absolute value of the double val */
double dabs(double val);

#endif /* End of CALC_H */
