#include <stdio.h>

#include "coil.h"
#include "print.h"
#include "calc.h" 

void print_coil(struct coil_t *coil) {
    printf("\n");
    printf("-----------------------\n");
    printf("| Calculation results |\n");
    printf("-----------------------\n");
    print_value("Target inductance:", coil->inductance);
    print_value("Target inductance precision:", coil->precision);
    print_value("Wire diameter:", coil->w_diameter);
    print_value("Inner coil diameter:", coil->c_in_diameter);
    if (coil->c_length_radius_ratio > 0) 
	printf("Target l/r ratio: %.2f\n", coil->c_length_radius_ratio);

    print_line();
    print_value("Calculated inductance:", coil->calc_inductance);
    print_value("Calculated inductance precision:", coil->calc_precision);
    
    print_line();
    printf("Number of turns: N = %d (%.2f)\n", 
	    coil->n_turns, coil->raw_n_turns);
    print_value("Coil thickness: c =", coil->c_thickness);
    if (coil->c_length.value > 0) 
        print_value("Coil length: l =", coil->c_length);
    print_value("Wire length needed: L =", coil->w_length);
    if (coil->c_length_radius_ratio > 0) 
	printf("Actual l/r ratio: %.2f\n", coil->c_calc_length_radius_ratio);
}

void print_value(char *msg, struct data_t data) {
    char *fmt;
    if (data.unit == HENRY)
	fmt = "%s %.3f %c%c\n";
    else
	fmt = "%s %.2f %c%c\n";

    printf(fmt, msg, data.value, print_multiplier(data), print_unit(data));

}

char print_multiplier(struct data_t data) {
    switch (data.multiplier) {
	case MILLI:
	    return 'm';
	case MICRO:
	    return 'u';
	case CENTI:
	    return 'c';
	case DECI:
	    return 'd';
	case ONE:
	default:
	    return '\0';
    }
}

char print_unit(struct data_t data) {
    switch (data.unit) {
	case HENRY:
	    return 'H';
	case METER:
	    return 'm';
	case PERCENT:
	    return '%';
	default:
	    return '\0';
    }
}

void print_line(void) {
    printf("------------------------------------------\n");
}
