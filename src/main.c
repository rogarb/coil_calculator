/*****************************************************************************\
 *									     *
 * Coil calculator using Brook formula:
 *	L = 400.PI.10^-9.r.N^2.[(0.5 + x/12).ln(8/x) - 0.84834 + 0.2041.x]
 *
 *	with
 *	    - L: inductance in Henry
 *	    - r: radius of the coil in meters
 *	    - N: number of wire turns
 *	    - x = (l/(2.r))^2 with l the coil length, equals to the coil 
 *				   thickness c
 *
 * considering that the section of the coil section is a square of height c
 * (so the length of the coil l is equal to its thickness c)
 *
 * Given the diameter of the wire d, we can write a relation between c, N and
 * d: 
 *	c = d.sqrt(N)
 *
 * we also calculate the mean radius r from the inner radius i:
 *	r = i + c/2
 *
 * Input data:
 *	Inner diameter of the coil (default unit m)
 *	Diameter of the wire (default unit in m)
 *	Target value of the coil (default in H)
 *
 * Output data
 *	Number of turns
 *	Thickness  of the coil in mm
 *
\*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "coil.h"
#include "calc.h"
#include "print.h"
#include "parser.h"

int main(void) {
    struct coil_t coil;
    char input_buf[BUFSIZ+1] = { '\0' };
    int formula = 0;
 
    printf(
"This program calculates the number of turns needed to obtain a particular\n"
"inductance value for air cored coils, given a set of input parameters.\n"
"It can calculate square section circular coils (using Brooks formula) and\n"
"solenoids (using Wheeler formula).\n"
//"recursively computes the inductance in order to determine 
"If no unit is given when entering the parameters, inductance is assumed to\n"
"be expressed in Henry(H), distances in meter(m) and precision in\n"
"percent(%%).\n"
"\n"
"Supported unit symbols: H (Henry), m (meter), %% (percent)\n"
"Supported unit modifiers: d (deci), c (centi), m (milli), u (micro)\n"
"\n"
"Use ctrl-c or ctrl-d for exiting\n\n");

    /* which type of coil should we calculate ? */
    while (formula <= 0 || formula >= NUM_FORMULA) {
	printf("Select the type of coil you want to calculate.\n"
	       "Available choices:\n"
	       "%d. Square section circular coil\n"
	       "%d. Single layer solenoid\n"
	       "%d. Multiple layer solenoid\n"
	       "Choice: ",
	       BROOKS, WHEELER_S, WHEELER_M);

	if (fgets(input_buf, sizeof(input_buf), stdin) == NULL) {
	    fprintf(stderr, "Error reading STDIN\n");
	    return -1;
	}

	formula = input_buf[0] - '0';

	if (strlen(input_buf) != 2 
		|| formula < 0
		|| formula > NUM_FORMULA) {
	    printf("Invalid input\n");
	}
    }

    /* get the inductance value */
    do {
	printf("Enter the target L value:\n");
    
	if (fgets(input_buf, sizeof(input_buf), stdin) == NULL) {
	    fprintf(stderr, "Error reading STDIN\n");
	    return -1;
	}
    } while (parse_input(&(coil.inductance), input_buf, HENRY));
    
    /* get the inner coil diameter value */
    do {
	printf("Enter the value of the coil inner diameter:\n");
    
	if (fgets(input_buf, sizeof(input_buf), stdin) == NULL) {
	    fprintf(stderr, "Error reading STDIN\n");
	    return -1;
	}
    } while (parse_input(&(coil.c_in_diameter), input_buf, METER));
    
    /* get the wire diameter value */
    do {
	printf("Enter the value of the wire diameter:\n");
    
	if (fgets(input_buf, sizeof(input_buf), stdin) == NULL) {
	    fprintf(stderr, "Error reading STDIN\n");
	    return -1;
	}
    } while (parse_input(&(coil.w_diameter), input_buf, METER));

    /* get the precision value */
    do {
	printf("Enter the required precision:\n");
    
	if (fgets(input_buf, sizeof(input_buf), stdin) == NULL) {
	    fprintf(stderr, "Error reading STDIN\n");
	    return -1;
	}
    } while (parse_input(&(coil.precision), input_buf, PERCENT));
    
    /* if we calculate a multiple layer coil, ask for length/radius ratio */
    if (formula == WHEELER_M) {
	coil.c_length_radius_ratio = -1;
	while (coil.c_length_radius_ratio <= 0) {
	    printf("Enter the maximum coil length / coil radius ratio:\n");
	
	    if (fgets(input_buf, sizeof(input_buf), stdin) == NULL) {
		fprintf(stderr, "Error reading STDIN\n");
		return -1;
	    }
	    
	    char *endptr = input_buf;

	    coil.c_length_radius_ratio = strtod(input_buf, &endptr);

	    if (endptr != &input_buf[strlen(input_buf)-1]
		|| coil.c_length_radius_ratio <= 0) {
		printf("Invalid input\n");
		coil.c_length_radius_ratio = -1;
		/* DEBUG */
		printf("endptr = 0x%p\n", endptr);
		printf("&input_buf[strlen(input_buf)-2] = 0x%p\n",
			&input_buf[strlen(input_buf)-2]);
	    }
	}
    }

    if (calculate_coil(&coil, formula)) {
	fprintf(stderr, "Error calculating coil values\n");
	return -1;
    } else {
	print_coil(&coil);
    }
    return 0;
}

