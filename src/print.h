/* print.h - definitions for printing functions */

#ifndef PRINT_H
#define PRINT_H

#include "coil.h"

/* these functions return the proper character to print or '\0' if none */
char print_multiplier(struct data_t data);
char print_unit(struct data_t data);

void print_value(char *msg, struct data_t data);
void print_coil(struct coil_t *coil);
void print_line(void);

#endif /* End of PRINT_H */
