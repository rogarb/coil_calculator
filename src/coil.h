/* coil.h - defines the structures related to the coil to be calculated */

#ifndef COIL_H
#define COIL_H

struct data_t {
    double value;   /* raw value */
    int unit;	    /* code for unit */
    int multiplier;
};

struct coil_t {
    struct data_t inductance;		/* target inductance in Henry */
    struct data_t w_diameter;		/* wire diameter in m */
    struct data_t w_length;		/* wire length in m */
    struct data_t c_in_diameter;	/* inner diameter of the coil in m */
    struct data_t c_thickness;		/* coil thickness in m */
    struct data_t c_length;		/* coil length in m */
    struct data_t precision;	    /* precision expected for the calculation */
    struct data_t calc_inductance;	/* calculated inductance in Henry */
    struct data_t calc_precision;	/* calculated precision in % */
    double c_length_radius_ratio;	/* coil length/radius (avg) ratio */
    double c_calc_length_radius_ratio;	/* actual coil length/radius ratio */
    double raw_n_turns;			/* raw number of turns */
    int	n_turns;			/* entire number of turns */
    
    
};

enum unit_multiplier { CENTI, DECI, MILLI, MICRO, ONE, MULT_SIZE };

enum unit_type { HENRY, METER, PERCENT, UNIT_TYPE_SIZE };

extern const int DEFAULT_MULT;

/* data validation functions: they return 1 if true, 0 if false */
int is_valid_unit(int unit);
int is_valid_multiplier(int multiplier);

#endif /* End of COIL_H */
