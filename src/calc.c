/* calc.c - implementation of calculation functions */

#include <stdio.h>
#include <math.h>

#include "calc.h"
#include "coil.h"
#include "print.h"

const int MAX_PASS = 10000;   /* max iterations in coil calculation */

double modifier_value(const int modifier) {
    switch (modifier) {
	case DECI:
	    return 0.1;
	case CENTI:
	    return 1e-2;
	case MILLI:
	    return 1e-3;
	case MICRO:
	    return 1e-6;
	case ONE:
	default:
	    return 1.0;
    }
}

int convert(struct data_t *data, const int to_mod) {
    if (! is_valid_multiplier(to_mod)) {
	fprintf(stderr, 
		"[BUG]: attempting to convert using an invalid modifier\n"
		"No conversion done\n");
	return -1;
    }

    data->value = ((data->value * modifier_value(data->multiplier)) 
	    / modifier_value(to_mod));
    data->multiplier = to_mod;
    
    return 0;
}

double calculate(const struct data_t data, const int to_modifier) {
    if (! is_valid_multiplier(to_modifier))
	fprintf(stderr, 
		"[BUG]: attempting to convert using an invalid modifier\n");

    return ((data.value * modifier_value(data.multiplier)) 
	    / modifier_value(to_modifier));
}

double dabs(double val) {
    return (val < 0 ? -val : val);
}

int calculate_coil(struct coil_t *coil, enum FORMULA formula) {
    /* target inductance in Henry */
    const double target_inductance_H = calculate(coil->inductance, ONE);
    /* wire diameter in meter */
    const double w_diameter_m = calculate(coil->w_diameter, ONE);
    /* coil inner diameter in meter */
    const double c_in_diameter_m = calculate(coil->c_in_diameter, ONE);
    /* absolute target precision in Henry */
    const double precision_H = 
	(target_inductance_H * coil->precision.value) / 100; 
    /* max solenoid length for multiple layer solenoid calculation */
    const double c_length_max_m = 10 * c_in_diameter_m / 2;
    
    const double MAX_STEP = 50.0;
    const double MIN_STEP = 1.0;

    double n_steps = MAX_STEP;
    double calculated_inductance_H = -1;
    double x = 0;
    double c_thickness_m = 0;
    double prev_n_turn = 0;
    double prev_prev_n_turn = 0;
    double raw_n_turns = 100.0;
    double avg_radius_m = 0;
    double c_length_m = 0;
    int pass = 0;
    int ret = 0;
    int oscill = 0; 

    coil->c_length.value = -1;
        
    print_line();
    printf("Starting calculation....\n");
    do {
	pass++;
	prev_prev_n_turn = prev_n_turn;
	prev_n_turn = raw_n_turns;

	/* check that N is still greater than n_steps (N > 0!!!) */
	while (raw_n_turns <= n_steps || n_steps > MIN_STEP)
	    n_steps /= 2;

	/* guess N if we didn't converge */
	if (calculated_inductance_H != -1) {
	    if (calculated_inductance_H > target_inductance_H)
		raw_n_turns -= n_steps;
	    else
		raw_n_turns += n_steps;
	}

	/* if we oscillate between 2 values we decrease the steps */
	if (prev_prev_n_turn == raw_n_turns) {
	    if (n_steps > MIN_STEP) {
		n_steps /= 2;
	    } else {
		oscill = 1;
		break;
	    }
	}

	/* the stepping shouldn't go below the minimum value */
	if (n_steps < MIN_STEP)
	    n_steps = MIN_STEP;

	if (formula == BROOKS) {
/* Brook formula:
 *	L = 400.PI.10^-9.r.N^2.[(0.5 + x/12).ln(8/x) - 0.84834 + 0.2041.x]
 *
 *	with
 *	    - L: inductance in Henry
 *	    - r: radius of the coil in meters
 *	    - N: number of wire turns
 *	    - x = (l/(2.r))^2 with l the coil length, equals to the coil 
 *				   thickness c
 *
 * considering that the section of the coil section is a square of height c
 * (so the length of the coil l is equal to its thickness c)
 *
 * Given the diameter of the wire d, we can write a relation between c, N and
 * d: 
 *	c = d.sqrt(N)
 *
 * mean radius r from the inner radius i:
 *	r = i + c/2
 *
 */
	    /* calculate x */
	    c_thickness_m = w_diameter_m * sqrt(raw_n_turns);
	    avg_radius_m = (c_in_diameter_m + c_thickness_m) / 2;
	    x = pow(c_thickness_m/(2*avg_radius_m), 2);

	    /* calculate the inductance */
	    calculated_inductance_H = 
		400 * M_PI * 1e-9 * avg_radius_m * pow(raw_n_turns, 2) *
		((0.5 + x/12)*log2(8/x) - 0.84834 + 0.2041*x);
	} else if (formula == WHEELER_S) {
	    /* this shouldn't change at every iteration but we leave it 
	     * here for the sake of code organization */
	    avg_radius_m = (c_in_diameter_m + w_diameter_m) / 2;
	    c_thickness_m = w_diameter_m;

	    /* calculate inductance */
	    calculated_inductance_H = 
		4e-6 * pow(avg_radius_m, 2) * pow(raw_n_turns, 2)
		/ (0.9 * avg_radius_m + raw_n_turns * w_diameter_m);
	} else if (formula == WHEELER_M) {
/* Multi layer air core solenoid
 * 
 * L {uH} = 31.6 * N^2 * r1^2 / (6*r1 + 9*L + 10*(r2-r1))
 * 
 *	L{uH} = Inductance in microHenries
 * 	N^2 = Total Number of turns on coil Squared
 * 	r1 = Radius of the inside of the coil {meters}
 * 	r2 = Radius of the outside of the coil {meters}
 * 	L = Length of the coil {meters}
 */
	    c_length_m = raw_n_turns * w_diameter_m;
	    c_thickness_m = w_diameter_m;

	    /* repack the coil if too long */
	    if (c_length_m > c_length_max_m) {
		c_thickness_m = (c_length_m / c_length_max_m) 
				* w_diameter_m;
		c_length_m = c_length_max_m;
	    }
	    
	    calculated_inductance_H = 
		31.6e-6 * pow(raw_n_turns, 2) * pow(c_in_diameter_m/2, 2)
		/ (3 * c_in_diameter_m + 9 * c_length_m + 10 * c_thickness_m);

	    avg_radius_m = (c_in_diameter_m + c_thickness_m) / 2;

	    coil->c_calc_length_radius_ratio = c_length_m / avg_radius_m;
	} else {
	    printf("[BUG]: Wrong formula parameter for calculation\n");
	    return -1;
	}

#ifdef DEBUG
	printf("Pass: %d\n", pass);
	printf("N = %f\n", raw_n_turns);
	printf("Target L = %f H\n", target_inductance_H);
	printf("Calc L = %f H\n", calculated_inductance_H);
	printf("Precision (target) = %f (%f)\n", 
		dabs(target_inductance_H - calculated_inductance_H),
		precision_H);
	print_line();
#endif

    } while (dabs(calculated_inductance_H - target_inductance_H) > precision_H
	    && pass < MAX_PASS);

    if (pass < MAX_PASS && !oscill) {
	printf("Calculation converged after %d iterations\n", pass);
	coil->raw_n_turns = raw_n_turns;
	coil->n_turns = (int) raw_n_turns;

	coil->c_thickness.value = c_thickness_m;
	coil->c_thickness.multiplier = ONE;
	coil->c_thickness.unit = METER;

	coil->calc_inductance.value = calculated_inductance_H;
	coil->calc_inductance.multiplier = ONE;
	coil->calc_inductance.unit = HENRY;

	coil->calc_precision.value = 
	    100 * dabs(calculated_inductance_H - target_inductance_H) 
	    / target_inductance_H;
	coil->calc_precision.multiplier = ONE;
	coil->calc_precision.unit = PERCENT;

	coil->w_length.value = 2 * M_PI * avg_radius_m * raw_n_turns;
	coil->w_length.multiplier = ONE;
	coil->w_length.unit = METER;

	/* convert calculated inductance, coil thickness, coil length
	 * and wire length */ 
	if (convert(&(coil->calc_inductance), coil->inductance.multiplier) 
	 || convert(&(coil->c_thickness), MILLI)
	 || convert(&(coil->c_length), CENTI))
	    ret = -1;

    } else {
	if (pass >= MAX_PASS)
	    printf("Max iterations attained, calculation didn't converge\n");
	if (oscill)
		printf("Calculation is oscillating and won't converge\n");

	printf("Check input parameters\n");
	/*  especially wire diameter against precision and target L */
	printf("N = %f\n", raw_n_turns);
	printf("Target L = %f H\n", target_inductance_H);
	printf("Calc L = %f H\n", calculated_inductance_H);
	printf("Precision (target) = %f (%f)\n", 
		dabs(target_inductance_H - calculated_inductance_H),
		precision_H);
	ret = -1;
    } 

    print_line();
    return ret;
}

