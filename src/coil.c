#include "coil.h"

const int DEFAULT_MULT = ONE; /* sets the default multplier to 1, i.e. input 
			       * is treated as Henry or meter if no unit is
			       * specified */

int is_valid_unit(int unit) {
    if (unit < 0  || unit >= UNIT_TYPE_SIZE)
	return 0;
    else 
	return 1;
}

int is_valid_multiplier(int multiplier) {
    if (multiplier < 0  || multiplier >= MULT_SIZE)
	return 0;
    else 
	return 1;
}

