/* parser.h - definition of input parser functions */

#ifndef PARSER_H
#define PARSER_H

#include "coil.h"

/* returns 0 on success, < 0 on failure */
int parse_input(struct data_t *data, char *input, int expected_data_unit);

#endif /* End of PARSER_H */
