#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "parser.h"
#include "coil.h"

int parse_input(struct data_t *data, char *input, int expected_data_unit) {
    /* input is assumed to be a '\0' terminated string !!! */
    int ret = 0;
    char *unit_p = NULL;

    /* reset the given data pointer */
    data->value = -1.0;
    data->multiplier = -1;
    data->unit = -1;

    /* we ignore the blank characters */
    while (isblank(*input))
	input++;

    unit_p = input;

    /* find the first occurence of non numeric or . character */
    while (isdigit(*unit_p) || *unit_p == '.')
	unit_p++;

    /* we ignore the blank characters */
    while (isblank(*unit_p))
	unit_p++;

    if (unit_p != input) {
	/* we parse the optional unit field or set it to its default */
	if (*unit_p == '\n' || *unit_p == '\0') {
	    data->unit = expected_data_unit;
	    data->multiplier = DEFAULT_MULT;
	} else {
	    /* we expect maximum two chars */
	    char mult_c = *unit_p;
	    char unit_c = *(unit_p+1);

	    if (unit_c == '\n' || unit_c == '\0') {
		unit_c = mult_c;
		mult_c = 0;
	    }

	    if (mult_c) {
		switch (mult_c) {
		    case 'd':
			data->multiplier = DECI;
			break;
		    case 'c':
			data->multiplier = CENTI;
			break;
		    case 'm':
			data->multiplier = MILLI;
			break;
		    case 'u':
			data->multiplier = MICRO;
			break;
		    default:
			data->multiplier = -1;
			break;
		}
	    } else {
		data->multiplier = ONE;
	    }

	    switch (unit_c) {
		case 'H':
		    data->unit = HENRY;
		    break;
		case 'm':
		    data->unit = METER;
		    break;
		case '%':
		    data->unit = PERCENT;
		    if (data->multiplier != ONE) /* percent cannot have */
			data->multiplier = -1;	 /* a modifier */
		    break;
		default:
		    data->unit = -1;
		    break;
	    }
	}

	/* we separate the value from the unit if present */
	
	*unit_p = '\0';

	/* we then convert the value from ascii to double */
	data->value = atof(input);

    }

    /* we ensure written data is valid */
    if (is_valid_unit(data->unit) && data->unit == expected_data_unit
				  && is_valid_multiplier(data->multiplier)
				  && data->value > 0) {
	ret = 0;
    } else {
	ret = -1;
    }
    
    /* percentage value has to also be lower than 100 */
    if (data->unit == PERCENT && data->value >= 100)
	ret = -1;

    if (ret)
	fprintf(stderr, "Invalid input\n");

    return ret;
}

