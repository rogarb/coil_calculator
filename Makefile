CC=/usr/bin/gcc
CFLAGS=-O2 -Wall -pedantic
INC=-Isrc
DBG=-DDEBUG -g
LDFLAGS=-lm
SRC=$(wildcard src/*.c)
EXE=coil_calculator
OBJ=$(subst .c,.o,$(SRC))
OBJ_WITHOUT_MAIN=$(subst src/main.o,,$(OBJ))
TEST_SRC=$(wildcard tests/*.c)
TEST_OBJ=$(subst .c,.o,$(TEST_SRC))
TEST_EXE=$(subst .c,,$(TEST_SRC))

$(EXE): $(OBJ)
	${CC} $(CFLAGS) $(LDFLAGS) -o $(EXE) $(OBJ)

.PHONY: debug
debug: CFLAGS+=$(DBG)
debug: $(EXE)

.PHONY:all_debug
all_debug: CFLAGS+=$(DBG)
all_debug: tests $(EXE)

.PHONY:all
all: tests $(EXE)

.PHONY: tests
tests: $(TEST_EXE)

tests/%.o: tests/%.c
	${CC} $(CFLAGS) $(INC) $(LDFLAGS) -o $@ -c $<

tests/%: tests/%.o $(OBJ_WITHOUT_MAIN)
	${CC} $(CFLAGS) $(INC) $(LDFLAGS) -o $@ $(OBJ_WITHOUT_MAIN) $<

.PHONY: clean
clean:
	rm -f $(EXE) $(OBJ) $(TEST_OBJ) $(TEST_EXE)

