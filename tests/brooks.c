/* test for Brooks equation using fixed parameters */
#include <stdio.h>

#include "coil.h"
#include "calc.h"
#include "print.h"

int main(void) {
    struct coil_t coil;

    printf("Test for Brooks equation\n");
    print_line();
    
    /* fill target inductance to 2.5mH */
    coil.inductance.value = 2.5;
    coil.inductance.unit = HENRY;
    coil.inductance.multiplier = MILLI;

    /* fill wire diameter to 0.5mm */
    coil.w_diameter.value = 0.5;
    coil.w_diameter.unit = METER;
    coil.w_diameter.multiplier = MILLI;

    /* fill coil diameter to 20cm */
    coil.c_in_diameter.value = 20.0;
    coil.c_in_diameter.unit = METER;
    coil.c_in_diameter.multiplier = CENTI;

    /* fill precision to 1% */
    coil.precision.value = 1.0;
    coil.precision.unit = PERCENT;
    coil.precision.multiplier = ONE;

    /* fill max length/radius ratio to -1 */
    coil.c_length_radius_ratio = -1.0;
    
    /* calculate using Brooks formula */
    if (calculate_coil(&coil, BROOKS)) {
	fprintf(stderr, "Error calculating coil values\n");
	return -1;
    } else {
	/* print result */
	print_coil(&coil);
    }


    return 0;
}
