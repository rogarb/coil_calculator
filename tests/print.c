/* test for print_coil function */
#include <stdio.h>

#include "coil.h"
#include "print.h"

int main(void) {
    struct coil_t coil;

    printf("Test for wheeler formulas\n");
    print_line();
    
    /* fill target inductance to 2.5mH */
    coil.inductance.value = 2.5;
    coil.inductance.unit = HENRY;
    coil.inductance.multiplier = MILLI;

    /* fill wire diameter to 0.5mm */
    coil.w_diameter.value = 0.5;
    coil.w_diameter.unit = METER;
    coil.w_diameter.multiplier = MILLI;

    /* fill wire length to 30m */
    coil.w_length.value = 30.0;
    coil.w_length.unit = METER;
    coil.w_length.multiplier = ONE;

    /* fill coil diameter to 2cm */
    coil.c_in_diameter.value = 2.0;
    coil.c_in_diameter.unit = METER;
    coil.c_in_diameter.multiplier = CENTI;

    /* fill coil thickness to 10mm */
    coil.c_thickness.value = 10.0;
    coil.c_thickness.unit = METER;
    coil.c_thickness.multiplier = MILLI;

    /* fill coil length to 10cm */
    coil.c_length.value = 10.0;
    coil.c_length.unit = METER;
    coil.c_length.multiplier = CENTI;

    /* fill precision to 1% */
    coil.precision.value = 1.0;
    coil.precision.unit = PERCENT;
    coil.precision.multiplier = ONE;

    /* fill calculated inductance to 2.495mH */
    coil.calc_inductance.value = 2.495;
    coil.calc_inductance.unit = HENRY;
    coil.calc_inductance.multiplier = MILLI;

    /* fill calculated precision to 0.32% */
    coil.calc_precision.value = 0.32;
    coil.calc_precision.unit = PERCENT;
    coil.calc_precision.multiplier = ONE;

    /* fill max length/radius ratio to 10 */
    coil.c_length_radius_ratio = 10.0;

    /* fill calculated length/radius ratio to 10 */
    coil.c_calc_length_radius_ratio = 9.3;
    
    /* fill number of turns to 192 */
    coil.raw_n_turns = 192.0;
    coil.n_turns = (int) coil.raw_n_turns;

    /* print result */
    print_coil(&coil);
    return 0;
}
