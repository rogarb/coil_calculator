/* test for wheeler equations using fixed parameters */
#include <stdio.h>

#include "coil.h"
#include "calc.h"
#include "print.h"

int main(void) {
    struct coil_t coil;

    printf("Test for wheeler formulas\n");
    print_line();
    
    /* fill target inductance to 2.5mH */
    coil.inductance.value = 2.5;
    coil.inductance.unit = HENRY;
    coil.inductance.multiplier = MILLI;

    /* fill wire diameter to 0.5mm */
    coil.w_diameter.value = 0.5;
    coil.w_diameter.unit = METER;
    coil.w_diameter.multiplier = MILLI;

    /* fill coil diameter to 2cm */
    coil.c_in_diameter.value = 2.0;
    coil.c_in_diameter.unit = METER;
    coil.c_in_diameter.multiplier = CENTI;

    /* fill precision to 1% */
    coil.precision.value = 1.0;
    coil.precision.unit = PERCENT;
    coil.precision.multiplier = ONE;

    /* fill max length/radius ratio to 10 */
    coil.c_length_radius_ratio = 10.0;
    
    /* calculate using wheeler single layer formula */
    printf("Wheeler single layer\n");
    if (calculate_coil(&coil, WHEELER_S)) {
	fprintf(stderr, "Error calculating coil values\n");
	return -1;
    } else {
	/* print result */
	print_coil(&coil);
    }

    printf("\n");
    print_line();    
    /* calculate using wheeler multiple layer formula */
    printf("Wheeler multiple layer\n");
    if (calculate_coil(&coil, WHEELER_M)) {
	fprintf(stderr, "Error calculating coil values\n");
	return -1;
    } else {
	/* print result */
	print_coil(&coil);
    }
    

    return 0;
}
