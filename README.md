 # Air cored coil calculator

 This is a calculator for square section circular inductors: given the diameter
 of the wire, the inner diameter of the coil, the desired inductance and the
 precision on the inductance, it gives the number of turns and the size of the 
 square section.

 It uses the Brook formula:
 	L = 400.PI.10^-9.r.N^2.[(0.5 + x/12).ln(8/x) - 0.84834 + 0.2041.x]
 
 	with
 	    - L: inductance in Henry
 	    - r: radius of the coil in meters
 	    - N: number of wire turns
 	    - x = (l/(2.r))^2 with l the coil length, equals to the coil 
 				   thickness c
 
 This formula is given in the Annex A of the book "Inside the metal detector"
 from George Overton and Carl Moreland.
